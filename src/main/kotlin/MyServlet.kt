import java.util.*
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "Phone Book", value = "/")
class MyServlet : HttpServlet() {

    companion object {
        val RESOURCE_PATH = "/home/user/IdeaProjects/lab-15/src/main/resources/"

        val USER = "user"

        fun sendResource(res: HttpServletResponse, resource: String) {
            res.writer.print(IO.readFile(RESOURCE_PATH + resource))
        }
    }

    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {

        println(req.requestURI)

        val session = req.getSession(true)

        // Check auth
        if (session.getAttribute(USER) == null) {
            // Hasn't auth, but maybe has cookie
            if (cookieLogin(req)) {
                println("Cookie login success")
                replyUser(req, res)
            } else {
                println("Guest session")
                replyGuest(req, res)
            }
        } else {
            println("Login success")
            replyUser(req, res)
        }
    }

    override fun doPost(req: HttpServletRequest, res: HttpServletResponse) {
        doGet(req, res)
    }

    private fun replyGuest(req: HttpServletRequest, res: HttpServletResponse) {
        when (req.requestURI) {
            "/"             -> sendResource(res, "index.html")
            "/login"        -> sendResource(res, "login.html")
            "/logininfo"    -> processLogin(req, res)
            "/signin"       -> sendResource(res, "signIn.html")
            "/signininfo"   -> processSingIn(req, res)
            "/ads.json"     -> sendResource(res, "ads.json")
            "/style.css"    -> sendResource(res, "style.css")
            "/scripts.js"   -> sendResource(res, "scripts.js")
            else            -> res.sendRedirect("/")
        }
    }

    private fun replyUser(req: HttpServletRequest, res: HttpServletResponse) {
        when (req.requestURI) {
            "/"             -> sendResource(res, "index.html")
            "/new"          -> sendResource(res, "adEditor.html")
            "/newad"        -> newAd(req, res)
            "/logout"       -> logout(req, res)
            "/ads.json"     -> sendResource(res, "ads.json")
            "/style.css"    -> sendResource(res, "style.css")
            "/scripts.js"   -> sendResource(res, "scripts.js")
            else            -> res.sendRedirect("/")
        }
    }

    private fun processSingIn(req: HttpServletRequest, res: HttpServletResponse) {
        val login = req.getParameter("login").trim()
        val pass = req.getParameter("pass").trim()
        val name = req.getParameter("name").trim()

        if (name != "" && login != "" && pass != "") {
            if (!IO.readUsers().any { it.login == login }) {
                val newUser = User(name, login, pass)
                IO.addUser(newUser)
                res.writer.write(0)
                return
            }
        }

        res.sendError(0)
    }

    private fun newAd(req: HttpServletRequest, res: HttpServletResponse) {
        println("HELLO WORKD")
        val sb = StringBuilder()
        val br = req.reader
        var line = br.readLine()
        while (line != null) {
            sb.append(line)
            line = br.readLine()
        }

        val text = sb.toString()

        println("STRING $text")

        if (text != "") {
            val user = req.session.getAttribute("user")
            println(user)
            if (user != null && user is User) {
                IO.addAd(Ad(Date().time, user.name, text))
                res.writer.write(0)
                return
            }
        }
        res.sendError(0)
    }

    private fun processLogin(req: HttpServletRequest, res: HttpServletResponse) {
        val session = req.getSession(true)
        val login = req.getParameter("login").trim()
        val pass = req.getParameter("pass").trim()

        val users = IO.readUsers()
        val user = users.find { it.login == login && it.password == pass }

        if (user != null) {
            session.setAttribute(USER, user)
            res.writer.print(authCookie(user))
        } else {
            res.status = 1
            res.sendError(0)
        }
    }

    private fun cookieLogin(req: HttpServletRequest): Boolean {
        val authCookie = req.cookies?.find { it.name == "auth" }
        if (authCookie != null) {
            val users = IO.readUsers()
            val user = users.find { it.hashCode() == authCookie.value.toInt() }
            if (user != null) {
                req.session.setAttribute(USER, user)
                return true
            }
        }
        return false
    }

    private fun logout(req: HttpServletRequest, res: HttpServletResponse) {
        req.session.removeAttribute(USER)
        res.sendRedirect("/")
    }

    private fun authCookie(u: User): String {
        return "{\"auth\":${u.hashCode()}}"
    }
}

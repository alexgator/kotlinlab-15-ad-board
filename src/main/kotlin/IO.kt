import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import java.io.File

class IO {
    companion object {
        private val gson = Gson()

        fun readFile(path: String): String {
            return File(path).readText()
        }

        private fun writeFile(path:String, text: String) {
            File(path).writeText(text)
        }


        private fun readAds(): List<Ad> {
            return gson.fromJson(readFile(MyServlet.RESOURCE_PATH + "ads.json"))
        }

        private fun writeAds(ads: List<Ad>) {
            writeFile(MyServlet.RESOURCE_PATH + "ads.json", gson.toJson(ads))
        }

        fun addAd(ad: Ad) {
            writeAds(readAds().plus(ad))
        }

        fun readUsers(): List<User> {
            return gson.fromJson(readFile(MyServlet.RESOURCE_PATH + "users.json"))
        }

        private fun writeUsers(user: List<User>) {
            writeFile(MyServlet.RESOURCE_PATH + "users.json", gson.toJson(user))
        }

        fun addUser(user: User) {
            writeUsers(readUsers().plus(user))
        }
    }
}
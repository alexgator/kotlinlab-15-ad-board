data class User(val name: String, val login: String, val password: String)

data class Ad(val id: Long, val user: String, val text: String)
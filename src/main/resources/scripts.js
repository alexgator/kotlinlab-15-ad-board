(function () {
    let authorized = getCookie("auth") != undefined;
    let path = document.location.pathname;

    if (authorized && path == "/login") {
        goto("/");
    }

    function create(elem) {
        return document.createElement(elem)
    }

    function get(query) {
        return document.querySelector(query);
    }

    function goto(href) {
        window.location.href = href;
    }

    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        let expires = options.expires;

        if (typeof expires == "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        let updatedCookie = name + "=" + value;

        for (let propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    function drawNavBar() {
        if (path != "/login" && path != "/signin") {
            let container = get(".nav");
            let authBtn = create("button");
            authBtn.innerText = authorized ? "Logout" : "Login";
            authBtn.addEventListener("click", () => {
                if (authorized) {
                    setCookie("auth", "", {
                        expires: -1
                    });
                    goto("logout");
                } else {
                    goto("login");
                }
            });
            container.appendChild(authBtn);
            if (!authorized) {
                let signinBtn = create("button");
                signinBtn.innerText = "Sign In";
                signinBtn.addEventListener("click", () => {
                    window.location.href = "/signin"
                });
                container.appendChild(signinBtn)
            }
        }
    }

    function createAd(user, text) {
        let ad = create("div");
        ad.className = "ad";
        let u = create("p");
        u.innerText = user;
        u.className = "ad-user";
        ad.appendChild(u);
        let t = create("p");
        t.innerText = text;
        t.className = "ad-text";
        ad.appendChild(t);
        return ad;
    }

    function drawAdBoard() {
        if (authorized) {
            get("#new-ad").classList.remove("hidden");
        }

        fetch("ads.json", {
            credentials: 'include'
        })
            .then((it) => {
                return it.json()
            })
            .then((ads) => {
                let boardContainer = get(".ad-container")

                ads.forEach((elem, index) => {
                    boardContainer.appendChild(createAd(elem.user, elem.text));
                })
            })

    }

    function addLoginEventListeners() {
        let btn = get("#auth-form-btn");
        btn.addEventListener("click", (e) => {
            e.preventDefault();
            let login = get("#auth-form-login").value.trim();
            let pass = get("#auth-form-pass").value.trim();
            console.log(`${login}, ${pass} `);

            fetch(`/logininfo?login=${login}&pass=${pass}`)
                .then((resp) => {
                    if (resp.ok) {
                        console.log("Logged in successfully");
                        resp.json()
                            .then((json) => {
                                document.cookie = `auth=${json.auth}`;
                                authorized = true;
                                window.location = "/"
                            })
                    } else {
                        console.log("Log in failed");
                        authorized = false;
                        setCookie("auth", "", {
                            expires: -1
                        })
                    }
                })
        })
    }
    
    function addSignInEventListeners() {
        let btn = get("#new-user-btn");

        btn.addEventListener("click", (e) => {
            e.preventDefault();
            let name = get("#new-user-name").value;
            let login = get("#new-user-login").value;
            let pass = get("#new-user-pass").value;
            if (name != undefined && login != undefined && pass != undefined) {
                fetch(`/signininfo?name=${name}&login=${login}&pass=${pass}`)
                    .then((resp) => {
                        console.log(resp)
                        if (resp.ok) {
                            goto("login")
                        } else {
                            get(".error").classList.remove("hidden")
                        }
                    })
            }
        })
    }

    function addNewAdEventListeners() {
        get("#new-ad-btn").addEventListener("click", (e) => {
            e.preventDefault();
            let text = get("#new-ad-text").value;

            console.log(text);

            fetch(`/newad`, {
                method: "POST",
                body: text,
                credentials: 'include'
            })
                .then(() => {
                    goto("/")
                })
        })

    }

    switch (path) {
        case "/":
            drawAdBoard();
            break;
        case "/login":
            addLoginEventListeners();
            break;
        case "/signin":
            addSignInEventListeners();
            break;
        case "/new":
            addNewAdEventListeners();
            break;
    }

    drawNavBar();
})();